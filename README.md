# README #

Event Driven Simulator.

### What is this repository for? ###

* Quick summary

This project is a Discrete Event Simulator. The programmer only needs to implement the LogicalProcess class and the EventHandler class with the needed processes and events.

For example, a process could be a "Car" and the events could be "Accelerate" and "Break". It is possible to instantiate multiple "Car"s, each one of them has intrinsically a state, when the a given event puts the the "Car" in the ending position, the simulation ends.

The programmer is responsible to output the statists that he/she sees to best fit the simulation.


* Version

0.0.1

### How do I get set up? ###

* Summary of set up

Download the repo. Create a ./build folder. Run 
```
#!bash

cmake ..
```
from the created build folder.

At this point the build folder has a Makefile (or other project) within. Run
```
#!bash

make
```
to build the project.

The library will be in a ./bin folder, not within, but in the same level than the ./build folder:
```
ProjectName
├── build
│   └── Makefile
└── bin
    └── lib.a
```
Also some examples will be there.

* Configuration

You may pass -DCMAKE_BUILD_TYPE=Debug to debug this project, basically some assert(<expr>) will be turned up.

When running the simulation, pass --print_stats to print some simulation related statistics, the output file can be changed using --stats_file "some.file".

* Dependencies

Only C++11 standard library.


### Contribution guidelines ###

* Writing tests

Some simulation tests are present in the test folder. The simulator itself relies on the correctness of the simulations.

* Code review

TODO

* Other guidelines

TODO

### Who do I talk to? ###

daniel_me@live.com.pt