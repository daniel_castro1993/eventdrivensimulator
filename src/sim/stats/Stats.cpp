#include "sim/stats/Stats.hpp"

static int inc_avg_s(sim::avg_s&, const double&);

sim::_avg_s::_avg_s() : avg(0), sq_stddev(0), count(0)
{
}

sim::_avg_s::_avg_s(const _avg_s& other) : avg(other.avg),
sq_stddev(other.sq_stddev), count(other.count)
{
}

sim::_avg_s::~_avg_s()
{
}

void sim::_avg_s::addNewVal(const double& new_val)
{
    inc_avg_s(*this, new_val);
}

void sim::_avg_s::reset()
{
    avg = 0;
    sq_stddev = 0;
    count = 0;
}

sim::_avg_s& sim::_avg_s::operator+=(const double& new_val)
{
    inc_avg_s(*this, new_val);
    return *this;
}

static int inc_avg_s(sim::avg_s& m_AVG_S, const double& m_NEW_VAL)
{
    double m_diff = (m_NEW_VAL) - m_AVG_S.avg;
    double m_count = m_AVG_S.count;
    if (m_AVG_S.count > 1) {
        double m_r = (m_count - 2) / (m_count - 1);
        double m_sq_stddev = m_AVG_S.sq_stddev;
        double m_sq_diff = m_diff * m_diff;
        m_AVG_S.sq_stddev = m_r * m_sq_stddev
                + m_sq_diff / m_count;
    }
    if (m_AVG_S.count > 0) {
        m_AVG_S.avg += m_diff / m_count;
    }
    else {
        m_AVG_S.avg = (m_NEW_VAL);
    }
    m_AVG_S.count++;
    return m_AVG_S.count;
}
