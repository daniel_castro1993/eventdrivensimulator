#include "EventDrivenSimulator.hpp"

using namespace sim;

EventHandler::EventHandler(lpid_s schd, simtime_s ts) : Taggable()
{
    now = ts;
    scheduler_process = schd;
}

EventHandler::EventHandler(lpid_s schd, simtime_s ts, int tags) : Taggable(tags)
{
    now = ts;
    scheduler_process = schd;
}

EventHandler::~EventHandler()
{
}

void EventHandler::Execute(LogicalProcess*, GlobalState*)
{
}

lpid_s EventHandler::GetSchedulerLP()
{
    return scheduler_process;
}

void EventHandler::SetSchedulerLP(lpid_s _sched)
{
    scheduler_process = _sched;
}

simtime_s EventHandler::GetTimestamp()
{
    return now;
}

void EventHandler::SetTimestamp(simtime_s _now)
{
    now = _now;
}
