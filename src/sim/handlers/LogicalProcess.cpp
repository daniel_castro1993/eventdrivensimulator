#include "EventDrivenSimulator.hpp"

#include <utility>
#include <set>

#include <typeindex>
#include <typeinfo>

using namespace sim;
using namespace std;

static vector<LogicalProcess*> s_instance_LPs;
static lpid_s _currentPid;
static simtime_s _currentTS;
static map<type_index, set<EventHandler*> > _scheduled_events;
static GlobalState* _global_state;
static int _global_state_size;

static void eventsAt(
	EventHandler*,
	vector<EventHandler*>&,
	function<void(vector<EventHandler*>&, EVENT_IT&, EventHandler*)>
);

static void eraseEvents(simtime_s);

LogicalProcess::LogicalProcess() : Taggable()
{
	_current_time = 0;
	_end = false;
}

LogicalProcess::~LogicalProcess()
{
}

void LogicalProcess::ScheduleEvent(EventHandler *e)
{
	assert(e->GetTimestamp() > _current_time);

	// TODO: insert in set might become heavy,
	//       maybe the check for repeated
	//       values can be done on cleanup
	_scheduled_events[typeid (*e)].insert(e);
	eventsAt(e, _events, []
			(vector<EventHandler*>& evts, EVENT_IT& it, EventHandler * ev)
			-> void
			{
				evts.insert(it, ev);
			});
}

void LogicalProcess::UnscheduleAllEvents()
{
	if (_currentPid == _id) {
		_events.erase(_events.begin() + 1, _events.end());
	}
	else {
		_events.clear();
	}
}

void LogicalProcess::UnscheduleAllEvents(int tags)
{
	EVENT_IT it,
			begin = _events.begin(),
			end = _events.end();
	vector<EventHandler*> new_events;

	if (_currentPid == _id && _events.size() < 2) {
		return;
	}
	else if (_currentPid == _id) {
		new_events.push_back(*begin);
		++begin;
	}

	for (it = begin; it != end; ++it) {
		if (!(*it)->HasAnyTags(tags)) {
			new_events.push_back(*it);
		}
	}
	_events.clear();
	_events.insert(_events.begin(), new_events.begin(), new_events.end());
}

LogicalProcess* LogicalProcess::getInstance(lpid_s id)
{
	assert(id > -1 && id < s_instance_LPs.size());
	return s_instance_LPs[id];
}

EventHandler* LogicalProcess::TopEvent()
{
	assert(!_events.empty());
	return _events.front();
}

simtime_s LogicalProcess::TopEventTS()
{
	assert(!_events.empty());
	return _events.front()->GetTimestamp();
}

bool LogicalProcess::HasNextEvent()
{
	return !_events.empty();
}

void LogicalProcess::ExecuteTopEvent()
{
	assert(!_events.empty());
	EVENT_IT begin = _events.begin();
	EventHandler *event = *begin;
	simtime_s ts = event->GetTimestamp();

	_currentPid = _id;
	_current_time = ts;
	_currentTS = ts;
	event->Execute(this, _global_state);
	begin = _events.begin(); // vector changed due to schedules

	_events.erase(begin);
}

bool LogicalProcess::IsEnd()
{
	return _end;
}

bool LogicalProcess::IsReset()
{
	return _reset;
}

void LogicalProcess::End()
{
	_end = true;
}

void LogicalProcess::Reset()
{
	CustomReset();
	_reset = true;
}

void LogicalProcess::CustomReset()
{
}

lpid_s LogicalProcess::GetId()
{
	return _id;
}

void LogicalProcess::SetId(lpid_s id)
{
	_id = id;
}

void LogicalProcess::setState(GlobalState* globalState, int size)
{
	_global_state = globalState;
	_global_state_size = size;
}

GlobalState* LogicalProcess::getState()
{
	return _global_state;
}

simtime_s LogicalProcess::GetCurrentTime()
{
	return _current_time;
}

lpid_s LogicalProcess::addLP(LogicalProcess *lp)
{
	int lp_id = s_instance_LPs.size();
	lp->SetId(lp_id);
	s_instance_LPs.push_back(lp);
	return lp_id;
}

void LogicalProcess::destroy()
{
	LP_IT it,
			begin = s_instance_LPs.begin(),
			end = s_instance_LPs.end();

	for (it = begin; it < end; ++it) {
		delete *it;
	}

	s_instance_LPs.clear();
	eraseEvents(DBL_MAX);

	delete _global_state;
}

int LogicalProcess::getNbLPs()
{
	return s_instance_LPs.size();
}

lpid_s LogicalProcess::currentLP()
{
	return _currentPid;
}

EventHandler* LogicalProcess::getRecyclable(type_index tid)
{
	set<EventHandler*>::iterator it, begin, end;

	if (_scheduled_events.find(tid) != _scheduled_events.end()) {
		begin = _scheduled_events[tid].begin();
		end = _scheduled_events[tid].end();
		for (it = begin; it != end; ++it) {
			if ((*it)->GetTimestamp() < _currentTS) {
				return *it;
			}
		}
	}
	return nullptr;
}

static void eventsAt(
	EventHandler *p,
	vector<EventHandler*>& events,
	function<void(vector<EventHandler*>&, EVENT_IT&, EventHandler*) > fnc
) {
	simtime_s ts, e_ts = p->GetTimestamp();
	EVENT_IT it,
			begin = events.begin(),
			end = events.end();

	for (it = begin; it != end; ++it) {
		ts = (*it)->GetTimestamp();
		if (e_ts < ts) {
			// insert the event here
			break;
		}
	}

	fnc(events, it, p);
}

static void eraseEvents(simtime_s currentTS)
{
	int i;
	map<type_index, set < EventHandler*>>::iterator it,
			begin_it = _scheduled_events.begin(),
			end_it = _scheduled_events.end();

	set<EventHandler*>::iterator s_it, s_begin_it, s_end_it;

	vector<map<type_index, set < EventHandler*>>::iterator> erase_it;
	vector<map<type_index, set < EventHandler*>>::iterator>::iterator v_it;

	for (it = begin_it; it != end_it; ++it) {
		s_begin_it = it->second.begin();
		s_end_it = it->second.end();
		set<EventHandler*> new_events;
		for (s_it = s_begin_it; s_it != s_end_it; ++s_it) {
			EventHandler *e = *s_it;
			if (e->GetTimestamp() < currentTS) {
				delete e;
			}
			else if (e->GetTimestamp() >= currentTS) {
				new_events.insert(e);
			}
		}
		it->second.clear();
		it->second.insert(new_events.begin(), new_events.end());
		if (it->second.empty()) {
			erase_it.push_back(it);
		}
	}
	for (v_it = erase_it.begin(); v_it != erase_it.end(); ++v_it) {
		_scheduled_events.erase(*v_it);
	}
}
