#include "sim/handlers/Taggable.h"

using namespace sim;

Taggable::Taggable()
{
    tags = 0;
}

Taggable::Taggable(int new_tags)
{
    tags = new_tags;
}

Taggable::~Taggable()
{
}

int Taggable::GetTags()
{
    return tags;
}

void Taggable::SetTags(int new_tags)
{
    tags = new_tags;
}

void Taggable::AddTags(int new_tags)
{
    tags |= new_tags;
}

void Taggable::RemoveTags(int new_tags)
{
    tags |= new_tags;
    tags ^= new_tags;
}

bool Taggable::HasAnyTags(int new_tags)
{
    return tags & new_tags;
}

