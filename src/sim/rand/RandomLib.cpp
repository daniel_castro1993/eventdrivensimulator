#include "EventDrivenSimulator.hpp"

#include <cassert>

using namespace std;
using namespace sim;

static vector<RandomLib*> s_instance_randomLib;
static int nbRandomLibs;

RandomLib* RandomLib::getInstance(int id)
{
    assert(id > -1 && id < s_instance_randomLib.size());

    return s_instance_randomLib[id];
}

int RandomLib::create(int seed)
{
    int id = s_instance_randomLib.size();

    s_instance_randomLib.push_back(new RandomLib(seed));

    return id;
}

void RandomLib::destroy()
{
    vector<RandomLib*>::iterator it,
            begin = s_instance_randomLib.begin(),
            end = s_instance_randomLib.end();

    for (it = begin; it < end; ++it) {
        delete *it;
    }

    s_instance_randomLib.clear();
}

int RandomLib::getCount()
{
    return s_instance_randomLib.size();
}

RandomLib::RandomLib(int seed)
{
    gen.seed(seed);
}

RandomLib::~RandomLib()
{
}

bool RandomLib::BernoulliDist(double prob)
{
    bernoulli_distribution::param_type new_p(prob);
    _bernoulliDist.param(new_p);
    return _bernoulliDist(gen);
}

int RandomLib::BinomialDist(double upper_bound, double prob)
{
    binomial_distribution<>::param_type new_p(upper_bound, prob);
    _binomialDist.param(new_p);
    return _binomialDist(gen);
}

int RandomLib::GeometricDist(double prob)
{
    geometric_distribution<>::param_type new_p(prob);
    _geometricDist.param(new_p);
    return _geometricDist(gen);
}

int RandomLib::NegativeBinomialDist(double k, double prob)
{
    negative_binomial_distribution<>::param_type new_p(prob);
    _negBinomialDist.param(new_p);
    return _negBinomialDist(gen);
}

int RandomLib::UniformIntDist(int min, int max)
{
    uniform_int_distribution<>::param_type new_p(min, max);
    _uniIntDist.param(new_p);
    return _uniIntDist(gen);
}

double RandomLib::UniformRealDist(double min, double max)
{
    uniform_real_distribution<>::param_type new_p(min, max);
    _uniRealDist.param(new_p);
    return _uniRealDist(gen);
}

double RandomLib::PoissonDist(double mean)
{
    poisson_distribution<>::param_type new_p(mean);
    if (_poissonDist.mean() != mean) {
        _poissonDist.param(new_p);
    }
    return _poissonDist(gen);
}

double RandomLib::ExpDist(double lambda)
{
    exponential_distribution<>::param_type new_p(lambda);
    _expDist.param(new_p);
    return _expDist(gen);
}

double RandomLib::GammaDist(double alpha, double beta)
{
    gamma_distribution<>::param_type new_p(alpha, beta);
    _gammaDist.param(new_p);
    return _gammaDist(gen);
}

double RandomLib::WeibullDist(double a, double b)
{
    weibull_distribution<>::param_type new_p(a, b);
    _weibullDist.param(new_p);
    return _weibullDist(gen);
}

double RandomLib::ExtremeValueDist(double a, double b)
{
    extreme_value_distribution<>::param_type new_p(a, b);
    _extremeValueDist.param(new_p);
    return _extremeValueDist(gen);
}

double RandomLib::NormalDist(double mean, double stddev)
{
    normal_distribution<>::param_type new_p(mean, stddev);
    _normalDist.param(new_p);
    return _normalDist(gen);
}

double RandomLib::LogNormalDist(double mean, double stddev)
{
    lognormal_distribution<>::param_type new_p(mean, stddev);
    _logNormalDist.param(new_p);
    return _logNormalDist(gen);
}

double RandomLib::ChiSquaredDist(double degrees)
{
    chi_squared_distribution<>::param_type new_p(degrees);
    _chiSquaredDist.param(new_p);
    return _chiSquaredDist(gen);
}

double RandomLib::CauchyDist(double a, double b)
{
    cauchy_distribution<>::param_type new_p(a, b);
    _cauchyDist.param(new_p);
    return _cauchyDist(gen);
}

double RandomLib::FisherFDist(double m, double n)
{
    fisher_f_distribution<>::param_type new_p(m, n);
    _fisherFDist.param(new_p);
    return _fisherFDist(gen);
}

double RandomLib::StudentTDist(double degrees)
{
    student_t_distribution<>::param_type new_p(degrees);
    _studentTDist.param(new_p);
    return _studentTDist(gen);
}
