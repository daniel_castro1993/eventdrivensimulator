#include "EventDrivenSimulator.hpp"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <ctime>
#include <queue>

#define PRINT_STATS_PARAM "--print_stats"
#define STATS_MODE_PARAM "--stats_mode"
#define STATS_FILE_PARAM "--stats_file"

using namespace std;
using namespace sim;

// Simulation parameters
static bool print_stats = false;
static STATS_MODE stats_mode = VERBOSE;
static string stats_file = "stats_output.out";
static FILE *stats_fp = NULL;
static int num_events = 0;

static vector<avg_s> events_duration;
static clock_t prev_clock, next_clock;
static simtime_s _cur_time;

static map<string, string> processedArgs;

static void parseArgs(int argc, char **argv);
static bool processLPs();
static void printStats();

extern void init(map<string, string>& args);
extern void end(vector<LogicalProcess*>, GlobalState*);

int reset_threshold = -1;
int end_threshold = -1;

/** Main routine.
 *
 * This is the entry point for every simulation.
 */
int main(int argc, char **argv)
{
	vector<LogicalProcess*> lps;
	lpid_s i;

	parseArgs(argc, argv);
	init(processedArgs);
	prev_clock = clock();
	while (processLPs());
	next_clock = clock();
	printStats();

	for (i = 0; i < LogicalProcess::getNbLPs(); ++i) {
		LogicalProcess *lp = LogicalProcess::getInstance(i);
		lps.push_back(lp);
	}

	end(lps, LogicalProcess::getState());
	LogicalProcess::destroy();
	RandomLib::destroy();
	if (stats_fp != NULL) {
		fclose(stats_fp);
	}
	return 0;
}

bool is_print_stats()
{
	return print_stats;
}

STATS_MODE get_stats_mode()
{
	return stats_mode;
}

string get_stats_file()
{
	return stats_file;
}

lpid_s sim::add_logical_process(LogicalProcess *lp)
{
	if (print_stats) {
		events_duration.push_back(avg_s());
	}
	return LogicalProcess::addLP(lp);
}

void sim::schedule_event(lpid_s id, EventHandler *event)
{
	assert(id > -1 && id < LogicalProcess::getNbLPs());
	LogicalProcess *lp = LogicalProcess::getInstance(id);
	lp->ScheduleEvent(event);
}

void sim::broadcast_event(EventHandler *event)
{
	int i;
	for (i = 0; i < LogicalProcess::getNbLPs(); ++i) {
		LogicalProcess *lp = LogicalProcess::getInstance(i);
		lp->ScheduleEvent(event);
	}
}

void sim::broadcast_event(int tags, EventHandler *event)
{
	int i;
	for (i = 0; i < LogicalProcess::getNbLPs(); ++i) {
		LogicalProcess *lp = LogicalProcess::getInstance(i);
		if (lp->HasAnyTags(tags)) {
			lp->ScheduleEvent(event);
		}
	}
}

void sim::unschedule_all_events(lpid_s id)
{
	assert(id > -1 && id < LogicalProcess::getNbLPs());
	LogicalProcess *lp = LogicalProcess::getInstance(id);
	lp->UnscheduleAllEvents();
}

void sim::unschedule_all_events(lpid_s id, int tags)
{
	assert(id > -1 && id < LogicalProcess::getNbLPs());
	LogicalProcess *lp = LogicalProcess::getInstance(id);
	lp->UnscheduleAllEvents(tags);
}

EventHandler* sim::recycle_event(type_index tid, lpid_s lpid, simtime_s ts)
{
	return recycle_event(tid, lpid, ts, 0);
}

EventHandler* sim::recycle_event(type_index tid,
								 lpid_s lpid,
								 simtime_s ts,
								 int tag)
{
	EventHandler* ev = LogicalProcess::getRecyclable(tid);

	if (ev != nullptr) {
		ev->SetTimestamp(ts);
		ev->SetSchedulerLP(lpid);
		ev->SetTags(tag);
	}

	return ev;
}

static void parseArgs(int argc, char **argv)
{
	int i = 0;

	for (i = 1; i < argc; ++i) {
		if (strcmp(argv[i], PRINT_STATS_PARAM) == 0) {
			print_stats = true;
		}
		else if (strcmp(argv[i], STATS_MODE_PARAM) == 0) {
			if (i == argc - 1) {
				printf("Usage: %s <option>\n"
					"Available options: "
					"VERBOSE, MEDIUM, MINIMAL\n",
					STATS_MODE_PARAM);
				exit(EXIT_FAILURE);
			}
			else {
				i++;
				if (strcmp(argv[i], "VERBOSE") == 0) {
					stats_mode = VERBOSE;
				}
				else if (strcmp(argv[i], "MEDIUM") == 0) {
					stats_mode = MEDIUM;
				}
				else if (strcmp(argv[i], "MINIMAL") == 0) {
					stats_mode = VERBOSE;
				}
				else {
					printf("Usage: %s <option>\n"
						"Available options: "
						"VERBOSE, MEDIUM, MINIMAL\n",
						STATS_MODE_PARAM);
					exit(EXIT_FAILURE);
				}
			}
		}
		else if (strcmp(argv[i], STATS_FILE_PARAM) == 0) {
			if (i == argc - 1) {
				printf("Usage: %s <file_name>\n",
					STATS_FILE_PARAM);
				exit(EXIT_FAILURE);
			}
		}
		else {
			string arg(argv[i]);
			string delimiter("=");
			size_t posDelimiter = arg.find(delimiter);
			string token = arg.substr(0, posDelimiter);
			string val("");

			if (posDelimiter != string::npos) {
				val = arg.substr(posDelimiter+1, arg.length());
			} else {
				token = arg;
			}
			processedArgs[token] = val;
		}
	}

	if (print_stats) {
		map<string, string>::iterator it,
				it_begin = processedArgs.begin(),
				it_end = processedArgs.end();
		stats_fp = fopen(stats_file.c_str(), "w+");
		time_t rawtime;
		time(&rawtime);
		fprintf(stats_fp, "######################################\n");
		fprintf(stats_fp, "###  %s", ctime(&rawtime));
		fprintf(stats_fp, "######################################\n");
		fprintf(stats_fp, "Simulation options: PRINT_STATS=%i, "
				"STATS_MODE=%i\n", print_stats, stats_mode);
		fprintf(stats_fp, "Extra options: ");
		for (it = it_begin; it != it_end; ++it) {
			fprintf(stats_fp, "%s=%s ", it->first.c_str(),
					it->second.c_str());
		}

		fflush(stats_fp);
	}
}

static bool allEnded()
{
	int i, countLPs = 0, nbLPs = LogicalProcess::getNbLPs();

	for (i = 0; i < nbLPs; ++i) {
		LogicalProcess *lp = LogicalProcess::getInstance(i);

		if (lp->IsEnd()) {
			countLPs++;
		}
	}

	return countLPs == nbLPs;
}

static LogicalProcess* findNextLP()
{
	simtime_s minTS = 0;
	int i, pid = -1, nbLPs = LogicalProcess::getNbLPs();

	for (i = 0; i < nbLPs; ++i) {
		LogicalProcess *lp = LogicalProcess::getInstance(i);
		bool hasNext = lp->HasNextEvent();
		simtime_s next = hasNext ? lp->TopEventTS() : 0;

		if (hasNext && (pid == -1 || next < minTS)) {
			minTS = next;
			pid = i;
		}
	}

	return pid == -1 ? NULL : LogicalProcess::getInstance(pid);
}

static bool processLPs()
{
	LogicalProcess *lp = findNextLP();
	bool isEnd = allEnded();

	if (lp == NULL) { // works like isEnd
		return false;
	}

	if (!lp->IsReset() && num_events > reset_threshold) {
		lp->Reset();
	}

	if (lp->IsReset() && end_threshold > 0 &&
			num_events > end_threshold + reset_threshold) {
		lp->End();
	}

	if (isEnd) { // still events remaining, but asked to end simulation
		return false;
	}

	if (!isEnd && lp == NULL) {
		const char msg[] = "No more events to process";
		fprintf(stderr, "%s \n", msg);
		exit(EXIT_FAILURE);
	}
	else {
		simtime_s new_ts = lp->GetCurrentTime();
		lp->ExecuteTopEvent();
		num_events++;
		if (print_stats) {
			events_duration[lp->GetId()] += new_ts - _cur_time;
		}
		_cur_time = new_ts;
	}

	return true;
}

void printStats()
{
	int i, total_events = 0;
	if (print_stats) {
		double delta = (next_clock - prev_clock) / (double) CLOCKS_PER_SEC;
		fprintf(stats_fp, "\nTotal execution time: %f seconds\n", delta);
		fprintf(stats_fp, "Stats per Logical Process (LP): \n");

		for (i = 0; i < LogicalProcess::getNbLPs(); ++i) {
			int count = events_duration[i].count;
			double avg = events_duration[i].avg,
					stddev = sqrt(events_duration[i].sq_stddev),
					duration = delta / (double) count;
			total_events += count;

			fprintf(stats_fp, "LP[%4i]: nb events: %i, avg time between "
					"events (simulation): %f (?%f), avg execution time per "
					"event: %f seconds \n", i, count, avg, stddev, duration);
		}
		fprintf(stats_fp, "\nTotal events: %i\n", total_events);

		fflush(stats_fp);
	}
}
