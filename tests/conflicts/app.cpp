#include "sim/Simulator.hpp"

#include "app.hpp"

#include <ctime>
#include <cstdlib>
#include <cstdio>

using namespace sim;
using namespace std;

#define AVG_LAT   10.0

#define new_granules(tp, rand, avg_acq) \
    tp->nb_acqs = fmax(1.0f, rand->ExpDist(1.0 / avg_acq));\
    if (tp->nb_acqs > UPPER_BOUND_RATIO * avg_acq) {\
        tp->nb_acqs = UPPER_BOUND_RATIO * avg_acq;\
    }

#define new_per_g_time(tp, rand, avg_compl_time) \
    tp->per_granule_time = rand->ExpDist(1.0 / avg_compl_time) / tp->nb_acqs;\
    if (tp->nb_acqs > UPPER_BOUND_RATIO * avg_compl_time) {\
        tp->nb_acqs = UPPER_BOUND_RATIO * avg_compl_time;\
    }

#define UPPER_BOUND_RATIO 100

// entry point for the simulation

void init(map<string, string>& args)
{
  ThreadProcess *tLP;
  InitEvent *initEV;
	SimState *state = new SimState();
  int i;
  lpid_s lpid;

	SET_STATE(SimState, state);

  srand(time(NULL));

	state->seed = rand();
  state->nb_lps = 10;
  state->tot_nb_granules = 1000;
  state->tot_nb_samples = 10;
  state->avg_acquisitions = 10;
  state->avg_completion_time = 1;
  state->prob_write = 1.0;

  if (ARG_EXISTS(args, "help")) {
    printf("arguments: \n");
    printf("   DEFAULT_SEED : uses some defined seed for the random number generator \n");
    printf("   P_W          : probability of generate a write \n");
    printf("   LPS          : number of threads generating accesses \n");
    printf("   POOL_SIZE    : size of the memory pool \n");
    printf("   AVG_TIME     : average mean time to access a memory item (exponential dist.) \n");
    printf("   AVG_ACQS     : average memory accesses per transaction (exponential dist.) \n");
    printf("   SAMPLES      : number of transactions per thread \n");
    return;
  }

  if (ARG_EXISTS(args, "DEFAULT_SEED")) {
    ARG_FETCH(args, "DEFAULT_SEED", state->seed);
  }

  if (ARG_EXISTS(args, "P_W")) {
    ARG_FETCH(args, "P_W", state->prob_write);
  }

  if (ARG_EXISTS(args, "LPS")) {
    ARG_FETCH(args, "LPS", state->nb_lps);
  }

  if (ARG_EXISTS(args, "POOL_SIZE")) {
    ARG_FETCH(args, "POOL_SIZE", state->tot_nb_granules);
  }

  if (ARG_EXISTS(args, "AVG_TIME")) {
    ARG_FETCH(args, "AVG_TIME", state->avg_completion_time);
  }

  if (ARG_EXISTS(args, "AVG_ACQS")) {
    ARG_FETCH(args, "AVG_ACQS", state->avg_acquisitions);
  }

  if (ARG_EXISTS(args, "SAMPLES")) {
    ARG_FETCH(args, "SAMPLES", state->tot_nb_samples);
  }

  if (state->tot_nb_granules < state->avg_acquisitions) {
    printf("POOL_SIZE must be greater than AVG_ACQS\n");
    exit(EXIT_FAILURE);
  }

  printf("Started simulation with parameters: \nLPS = %i\nP_W = %.2f\n"
         "DEFAULT_SEED = %i\nPOOL_SIZE = %i\nAVG_TIME = %f\nAVG_ACQS = %f\n"
         "SAMPLES = %i\n", state->nb_lps, state->prob_write, state->seed,
         state->tot_nb_granules, state->avg_completion_time,
         state->avg_acquisitions, state->tot_nb_samples);

  RandomLib::create(state->seed);
  for (i = 0; i < state->nb_lps; ++i) {
    tLP = new ThreadProcess();
    lpid = add_logical_process(tLP);
    initEV = NEW_EVENT(InitEvent,
                  lpid,
                  NEXT_TIMESTAMP(0),
                  E_TAG_ACQUIRE);
    initEV->is_abort = false;
    schedule_event(lpid, initEV);
  }
}


void end(vector<LogicalProcess*>, GlobalState*)
{
}

// EVENT implementations

void InitEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
  ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
  SimState* st = dynamic_cast<SimState*> (state);
  RandomLib *r = RandomLib::getInstance(0);
  double next_ts = NEXT_TIMESTAMP(now);
  AcquireEvent *acqEV;
  int g;
  ACCESS_T access;

  if (tp->nb_sample > st->tot_nb_samples && !tp->IsEnd()) {
    printf("[LP %i] total: %i, commits: %i, aborts: %i, "
           "retries: %.3f (?%.3f)\n",
           lp->GetId(), tp->nb_sample, tp->nb_commits, tp->nb_aborts,
           tp->retries.avg, sqrt(tp->retries.sq_stddev));
    tp->End();
  }

  tp->nb_sample++;

  new_granules(tp, r, st->avg_acquisitions);
  new_per_g_time(tp, r, st->avg_completion_time);

  acqEV = NEW_EVENT(AcquireEvent,
                    lp->GetId(),
                    next_ts,
                    E_TAG_ACQUIRE);

  g = r->UniformIntDist(1, st->tot_nb_granules);
  access = r->BernoulliDist(st->prob_write) ? WRITE : READ;

  acqEV->g = g;
  acqEV->access = access;

  if (!is_abort) {
    tp->count_retries = 0;
  }

  schedule_event(lp->GetId(), acqEV);
}

void AcquireEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
  ThreadProcess* tp = dynamic_cast<ThreadProcess*> (lp);
  SimState* st = dynamic_cast<SimState*> (state);
  RandomLib *r = RandomLib::getInstance(0);
  double lat = r->ExpDist(1.0 / tp->per_granule_time);
  double next_ts = NEXT_TIMESTAMP(now + lat);
  AcquireEvent *acqEV;
  CheckConflictEvent *chkEV;
  InitEvent *initEV;
  int next_g;
  ACCESS_T next_access;

  if (tp->acquired.size() < tp->nb_acqs) {
    acqEV = NEW_EVENT(AcquireEvent,
                      lp->GetId(),
                      next_ts,
                      E_TAG_ACQUIRE);

    next_access = r->BernoulliDist(st->prob_write) ? WRITE : READ;
    do {
      next_g = r->UniformIntDist(1, st->tot_nb_granules);
    }
    while (tp->acquired.find(next_g) != tp->acquired.end());
    acqEV->g = next_g;
    acqEV->access = next_access;

    tp->acquired[g] = access; // this g, not the next one

    chkEV = NEW_EVENT(CheckConflictEvent,
                      lp->GetId(),
                      NEXT_TIMESTAMP(now),
                      0);

    chkEV->g = g;
    chkEV->access = access;
    broadcast_event(chkEV);

    schedule_event(lp->GetId(), acqEV);
  }
  else {
    tp->nb_commits++;
    tp->retries += tp->count_retries;
    tp->acquired.clear();

    initEV = NEW_EVENT(InitEvent,
                       lp->GetId(),
                       NEXT_TIMESTAMP(now),
                       0);
    initEV->is_abort = false;

    schedule_event(lp->GetId(), initEV);
  }
}

void CheckConflictEvent::Execute(LogicalProcess *lp, GlobalState *state)
{
  ThreadProcess *tp = dynamic_cast<ThreadProcess*> (lp);
  SimState* st = dynamic_cast<SimState*> (state);
  InitEvent *initEV;
  map<int, ACCESS_T>::iterator
    it  = tp->acquired.find(g),
    end = tp->acquired.end();

  if (lp->GetId() == scheduler_process) {
    return;
  }

  if (it != end && (it->second == WRITE || access == WRITE)) {
    tp->nb_aborts++;
    tp->count_retries++;
    tp->acquired.clear();
    unschedule_all_events(lp->GetId());

    initEV = NEW_EVENT(InitEvent,
                       lp->GetId(),
                       NEXT_TIMESTAMP(now),
                       0);
    initEV->is_abort = true;

    schedule_event(lp->GetId(), initEV);
  }
}
