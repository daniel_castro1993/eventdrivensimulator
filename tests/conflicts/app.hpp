#ifndef H_APP_GUARD_
#define H_APP_GUARD_

#include "EventDrivenSimulator.hpp"

#include <map>

#define INJECT_CONSTRUCTORS(type)\
	type(lpid_s id, simtime_s ts) : EventHandler(id, ts) { };\
	type(lpid_s id, simtime_s ts, int tags) : EventHandler(id, ts, tags) { };

using namespace sim;

enum EVENTS_TAGS
{
	E_TAG_ACQUIRE = 0b0001
};

enum ACCESS_T
{
	WRITE, READ
};

class InitEvent : public EventHandler
{
public:
	bool is_abort;

	INJECT_CONSTRUCTORS(InitEvent)

	void Execute(LogicalProcess*, GlobalState*);
};

class AcquireEvent : public EventHandler
{
public:
	int g;
	ACCESS_T access;

	INJECT_CONSTRUCTORS(AcquireEvent)

	void Execute(LogicalProcess*, GlobalState*);
};

class CheckConflictEvent : public EventHandler
{
public:
	int g;
	ACCESS_T access;

	INJECT_CONSTRUCTORS(CheckConflictEvent)

	void Execute(LogicalProcess*, GlobalState*);
};

class ThreadProcess : public LogicalProcess
{
public:
	map<int, ACCESS_T> acquired;
	int nb_acqs;
	double per_granule_time;
	int nb_sample;
	int nb_commits, nb_aborts, count_retries;
	avg_s retries;

	using LogicalProcess::LogicalProcess;
};

class SimState : public GlobalState
{
public:
	int tot_nb_granules, tot_nb_samples, seed, nb_lps;
	double avg_acquisitions, avg_completion_time, prob_write;
};

#endif /* end of include guard: H_APP_GUARD_ */
