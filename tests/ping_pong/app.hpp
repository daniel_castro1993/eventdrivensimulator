#ifndef H_APP_GUARD_
#define H_APP_GUARD_

#include "EventDrivenSimulator.hpp"

using namespace sim;

enum EVENTS_TAGS
{
	PING_TAG = 0b0001,
	PONG_TAG = 0b0010
};

// PING

class PingEvent : public EventHandler
{
public:
	simtime_s send_ts;

	PingEvent(lpid_s, simtime_s);
	PingEvent(lpid_s, simtime_s, int);
	void Execute(LogicalProcess*, GlobalState*);
};

class PingProcess : public LogicalProcess
{
public:
	int nbPings;
	avg_s latPing;

	PingProcess();
};

// PONG

class PongEvent : public EventHandler
{
public:
	simtime_s send_ts;

	PongEvent(lpid_s, simtime_s);
	PongEvent(lpid_s, simtime_s, int);
	void Execute(LogicalProcess*, GlobalState*);
};

class PongProcess : public LogicalProcess
{
public:
	int nbPongs;
	avg_s latPong;

	PongProcess();
};

// GLOBAL STATE

class SimState : public GlobalState
{
public:
	int number_ping_pongs, seed;
	double avg_lat;

};

#endif /* end of include guard: H_APP_GUARD_ */
