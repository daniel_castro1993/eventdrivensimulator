#include "sim/Simulator.hpp"

#include "app.hpp"

#include <ctime>
#include <cstdlib>
#include <cstdio> 

using namespace sim;
using namespace std;

#define NUM_PING_PONGS 10
#define AVG_LAT   10.0

// entry point for the simulation

void init(map<string, string>& args)
{
	PingProcess *pingLP = new PingProcess();
	PongProcess *pongLP = new PongProcess();
	SimState *state = new SimState();
	PingEvent *ping;
	double lat;
	
	SET_STATE(SimState, state);
	
	srand(time(NULL));
	
	state->seed = rand();
	state->number_ping_pongs = NUM_PING_PONGS;
	state->avg_lat = AVG_LAT;

	if (!args["DEFAULT_SEED"].empty()) {
		// this does not check if it is an integer
		state->seed = stoi(args["DEFAULT_SEED"]);
	}

	if (!args["NB_PING_PONGS"].empty()) {
		// this does not check if it is an integer
		state->number_ping_pongs = stoi(args["NB_PING_PONGS"]);
	}

	if (!args["AVG_LAT"].empty()) {
		// this does not check if it is an double
		state->avg_lat = stod(args["AVG_LAT"]);
	}

	RandomLib::create(state->seed);

	add_logical_process(pingLP);
	add_logical_process(pongLP);

	lat = RandomLib::getInstance(0)->ExpDist(1.0 / state->avg_lat);
	ping = NEW_EVENT(PingEvent, 1, lat, 0);
	ping->send_ts = 0;

	schedule_event(0, ping);
}

void end(vector<LogicalProcess*> lps, GlobalState*)
{
}

// PING

PingEvent::PingEvent(lpid_s schd, simtime_s ts) : EventHandler(schd, ts)
{
}

PingEvent::PingEvent(lpid_s schd, simtime_s ts, int tags)
: EventHandler(schd, ts, tags)
{
}

void PingEvent::Execute(LogicalProcess *pingLP, GlobalState *state)
{
	printf(" - Ping\n");

	PingProcess* p = dynamic_cast<PingProcess*> (pingLP);
	SimState* st = dynamic_cast<SimState*> (state);
	PongEvent *pong1, *pong2, *pong3;
	simtime_s delay1 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);
	simtime_s delay2 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);
	simtime_s delay3 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);

	// discards the following pings
	unschedule_all_events(pingLP->GetId(), PING_TAG);

	p->latPing += now - send_ts;
	pong1 = NEW_EVENT(PongEvent, pingLP->GetId(), now + delay1, PONG_TAG);
	pong2 = NEW_EVENT(PongEvent, pingLP->GetId(), now + delay2, PONG_TAG);
	pong3 = NEW_EVENT(PongEvent, pingLP->GetId(), now + delay3, PONG_TAG);
	pong1->send_ts = now;
	pong2->send_ts = now;
	pong3->send_ts = now;

	p->nbPings++;

	schedule_event(1, pong1);
	schedule_event(1, pong2);
	schedule_event(1, pong3);

	if (p->nbPings > st->number_ping_pongs) {
		printf(" >> Ping avg lat = %f(%f)\n", p->latPing.avg,
			   sqrt(p->latPing.sq_stddev));
		p->End();
	}
}

PingProcess::PingProcess() : LogicalProcess()
{
	nbPings = 0;
}


// PONG

PongEvent::PongEvent(lpid_s schd, simtime_s ts) : EventHandler(schd, ts)
{
}

PongEvent::PongEvent(lpid_s schd, simtime_s ts, int tags)
: EventHandler(schd, ts, tags)
{
}

void PongEvent::Execute(LogicalProcess *pongLP, GlobalState *state)
{
	printf(" -- Pong\n");

	PongProcess* p = dynamic_cast<PongProcess*> (pongLP);
	SimState* st = dynamic_cast<SimState*> (state);
	PingEvent *ping1, *ping2, *ping3;
	simtime_s delay1 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);
	simtime_s delay2 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);
	simtime_s delay3 = RandomLib::getInstance(0)->ExpDist(1.0 / st->avg_lat);

	// discards the following pongs
	unschedule_all_events(pongLP->GetId(), PONG_TAG);

	p->latPong += now - send_ts;
	ping1 = NEW_EVENT(PingEvent, pongLP->GetId(), now + delay1, PING_TAG);
	ping2 = NEW_EVENT(PingEvent, pongLP->GetId(), now + delay2, PING_TAG);
	ping3 = NEW_EVENT(PingEvent, pongLP->GetId(), now + delay3, PING_TAG);
	ping1->send_ts = now;
	ping2->send_ts = now;
	ping3->send_ts = now;

	p->nbPongs++;

	schedule_event(0, ping1);
	schedule_event(0, ping2);
	schedule_event(0, ping3);

	if (p->nbPongs > st->number_ping_pongs) {
		printf(" >> Pong avg lat = %f(%f)\n", p->latPong.avg,
			   sqrt(p->latPong.sq_stddev));
		p->End();
	}
}

PongProcess::PongProcess() : LogicalProcess()
{
	nbPongs = 0;
}

