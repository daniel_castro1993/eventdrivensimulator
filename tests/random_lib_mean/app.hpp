#ifndef H_APP_GUARD_
#define H_APP_GUARD_

#include "EventDrivenSimulator.hpp"

#define INJECT_CONSTRUCTORS(type)\
	type(lpid_s id, simtime_s ts) : EventHandler(id, ts) { };\
	type(lpid_s id, simtime_s ts, int tags) : EventHandler(id, ts, tags) { };

using namespace sim;

class RandEvent : public EventHandler {
public:

	using EventHandler::EventHandler;

	void Execute(LogicalProcess*, GlobalState*);
} ;

class RandProcess : public LogicalProcess {
public:
	using LogicalProcess::LogicalProcess;
} ;

#endif /* end of include guard: H_APP_GUARD_ */

