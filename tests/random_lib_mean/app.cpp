#include "app.hpp"

#define NUM_SAMPLES 1000

#define EXP_MEAN_1 3.212318997
#define EXP_MEAN_2 1.937193736

using namespace sim;
using namespace std;

int nb_lps = 10;

void init(map<string, string>& args)
{

    printf("#########################\n");
    printf("### TEST RANDOM LIB      \n");
    printf("#########################\n");

    RandomLib::create(743345234);
    RandomLib::create(123578144);
    RandomLib::create(934256892);
    RandomLib::create(526782189);

    assert(RandomLib::getCount() == 4);

    RandomLib *r1 = RandomLib::getInstance(0);
    RandomLib *r2 = RandomLib::getInstance(1);
    RandomLib *r3 = RandomLib::getInstance(1);

    avg_s rand1;
    avg_s rand2;
    avg_s rand3;
    avg_s rand4;
    avg_s rand5;

    avg_s rand6;
    avg_s rand7;
    avg_s rand8;

    int i;
    RandProcess *tLP;
    RandEvent *initEV;
    lpid_s lpid;

    for (int i = 0; i < NUM_SAMPLES; ++i) {
        rand1 += r1->ExpDist(1.0 / EXP_MEAN_1);
        rand2 += r1->ExpDist(1.0 / EXP_MEAN_2);
        rand4 += r1->NormalDist(EXP_MEAN_1, EXP_MEAN_2);
        rand3 += r2->ExpDist(1.0 / EXP_MEAN_1);
        rand5 += r2->NormalDist(EXP_MEAN_2, EXP_MEAN_1);

        rand6 += r3->GammaDist(EXP_MEAN_1, EXP_MEAN_2);
        rand7 += r3->PoissonDist(EXP_MEAN_2);
        rand8 += r3->LogNormalDist(EXP_MEAN_1, EXP_MEAN_2);
    }

    printf("MEAN1=%f, MEAN2=%f\n\n", EXP_MEAN_1, EXP_MEAN_2);

    printf("EXP RAND1=%f(?%f) \n", rand1.avg, sqrt(rand1.sq_stddev));
    printf("EXP RAND2=%f(?%f) \n", rand2.avg, sqrt(rand2.sq_stddev));
    printf("NOR RAND4=%f(?%f) \n\n", rand4.avg, sqrt(rand4.sq_stddev));

    printf("EXP_1 RAND=%f(?%f) \n", rand3.avg, sqrt(rand3.sq_stddev));
    printf("NOR_1 RAND=%f(?%f) \n\n", rand5.avg, sqrt(rand5.sq_stddev));

    printf("GAMMA RAND=%f(?%f) \n", rand6.avg, sqrt(rand6.sq_stddev));
    printf("POISSON RAND=%f(?%f) \n", rand7.avg, sqrt(rand7.sq_stddev));
    printf("LOG_NOR RAND=%f(?%f) \n", rand8.avg, sqrt(rand8.sq_stddev));

    RandomLib::destroy();
    RandomLib::create(934256892);

    for (i = 0; i < nb_lps; ++i) {
        tLP = new RandProcess();
        lpid = add_logical_process(tLP);
        initEV = NEW_EVENT(RandEvent, lpid, NEXT_TIMESTAMP(0), 0);
        schedule_event(lpid, initEV);
    }
}

void end(vector<LogicalProcess*>, GlobalState*)
{
}

void RandEvent::Execute(LogicalProcess *lp, GlobalState*)
{
    int randVal = RandomLib::getInstance(0)->UniformIntDist(0, 10000);

    printf("Process: %i, rand = %i \n", lp->GetId(), randVal);

    lp->End();
}
