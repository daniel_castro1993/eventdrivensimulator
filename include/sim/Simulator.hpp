#ifndef H_SIMULATOR_GUARD_
#define H_SIMULATOR_GUARD_

#include "sim/types.hpp"
#include "handlers/EventHandler.hpp"

#include <cfloat>
#include <cmath>
#include <functional>
#include <string>
#include <sstream>
#include <iostream>
#include <map>

#include <typeindex>

/** Check if argument is defined (use only on init).
 */
#define ARG_EXISTS(args, arg)\
    args.find(arg) != args.end()\
//

/** Fetch the value of the argument (require to use ARG_EXISTS before).
 */
 #define ARG_FETCH(args, arg, val) ({\
   val = sim::convert_arg<__typeof__(val)>(args[arg]);\
 })


/** Creates a new event.
 *
 * Either the new event is a recycled old event or an allocated new one. This
 * macro DOES NOT schedule the event! You must then call schedule_event(...).
 */
#define NEW_EVENT(type, schd, ts, tags) ({\
    type* inst;\
    inst = (type*) recycle_event(typeid (type), schd, ts, tags);\
    if (inst == nullptr) {\
        inst = new type(schd, ts, tags);\
    }\
    inst;\
})

/** Sets the global state of the application.
 *
 * Use it the store global information shared by all LPs.
 */
#define SET_STATE(type, state) ({\
    LogicalProcess::setState(state, sizeof(type));\
})

using namespace std;

namespace sim {

	class LogicalProcess;
	class EventHandler;

	enum STATS_MODE {
		VERBOSE = 0, MEDIUM, MINIMAL
	} ;

	int get_tot_nb_lps();
	bool is_print_stats();
	STATS_MODE get_stats_mode();
	string get_stats_file();

	/** This function must be implemented by the programmer.
	 *
	 * It initializes the simulation. The initial events must be scheduled
	 * here (using schedule_event). Also the creation and initialization of
	 * each Logical Process must be set here (using add_logical_process).
	 *
	 * You shouldn't use add_logical_process anywhere else in the
	 * simulation. However, the scheduled events should call schedule_event
	 * in order to schedule new events.
	 *
	 * @param args the arguments passed to the simulation
	 */
	// void init(map<string, string>& args);

	/** This function must be implemented by the programmer.
	 *
	 * It is called before the simulation is clean up and exit.
	 *
	 * @param a vector with all the current LPs.
	 * @param the provided GlobalState with SET_STATE(type, state),
	 *        NULL if not set.
	 */
	// void end(vector<LogicalProcess*>, GlobalState*);

	/**
	 * Use this variable to set the number of events to discard (optional).
	 *
	 * After the threshold is reached, CustomReset() is called in all LPs.
	 * If this is not defined (or is less than 0), then none event is
	 * discarded.
	 */
	//extern int reset_threshold;

	/**
	 * Use this variable to set the number of events to end (optional).
	 *
	 * After the threshold is reached, CustomReset() is called in all LPs.
	 * If this is not defined (or is less than 0), then none event is
	 * discarded.
	 */
	//extern int end_threshold;

	/** Adds the given Logical Process.
	 *
	 * The returned lpid_s are generated sequentially. After lpid 1 it is
	 * guaranteed to the next lpid to be 2. The programmer must use this
	 * information to schedule the events in the correct LPs.
	 *
	 * @return the lpid of the created Logical Process
	 */
	lpid_s add_logical_process(LogicalProcess*);

	/** Schedules the given event.
	 *
	 */
	void schedule_event(lpid_s, EventHandler*);

	void broadcast_event(EventHandler*);
	void broadcast_event(int, EventHandler*);

	void unschedule_all_events(lpid_s);
	void unschedule_all_events(lpid_s, int);

	EventHandler* recycle_event(type_index, lpid_s, simtime_s);
	EventHandler* recycle_event(type_index, lpid_s, simtime_s, int);

  template <typename T>
  inline T convert_arg(string &arg)
  {
    if (!arg.empty()) {
      T ret;
      istringstream iss(arg);
      if (arg.find("0x") != string::npos) {
        iss >> hex >> ret;
      } else if (arg.find("0b") != string::npos) {
        ret = (T)stoll(arg.substr(2, arg.length()), nullptr, 2);
      } else if (arg.find("0") != string::npos) {
        iss >> oct >> ret;
      } else {
        iss >> dec >> ret;
      }

      if (iss.fail()) {
        cout << "Convert error: cannot convert string '" << arg << "' to value" << endl;
        return T();
      }
      return ret;
    }
    return T();
  }
}

#endif /* end of include guard: H_SIMULATOR_GUARD_ */
