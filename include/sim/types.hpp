#ifndef H_TYPES_GUARD_
#define H_TYPES_GUARD_

#define NEXT_TIMESTAMP(ts) ({ std::nextafter(ts, DBL_MAX); })

namespace sim {
	typedef double simtime_s;
	typedef int lpid_s;
}

#endif /* end of include guard: H_TYPES_GUARD_ */
