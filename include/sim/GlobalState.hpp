#ifndef H_GLOBALSTATE_GUARD_
#define H_GLOBALSTATE_GUARD_

#include "sim/types.hpp"

namespace sim
{

	class GlobalState
	{
	public:
		virtual ~GlobalState();
	};
}

#endif /* H_GLOBALSTATE_GUARD_ */

