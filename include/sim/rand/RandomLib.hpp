#ifndef H_RANDOMLIB_GUARD_
#define H_RANDOMLIB_GUARD_

#include <random>

using namespace std;

namespace sim {

	class RandomLib {
	public:
		static RandomLib* getInstance(int id);
		static int create(int seed);
		static void destroy();
		static int getCount();

		~RandomLib();

		bool BernoulliDist(double prob);
		int BinomialDist(double upper_bound, double prob);
		int GeometricDist(double prob);
		int NegativeBinomialDist(double k, double prob);
		int UniformIntDist(int min, int max);
		double UniformRealDist(double min, double max);

		double PoissonDist(double mean);
		double ExpDist(double lambda); // lambda = 1.0 / mean
		double GammaDist(double alpha, double beta);
		double WeibullDist(double a, double b);
		double ExtremeValueDist(double a, double b);

		double NormalDist(double mean, double stddev);
		double LogNormalDist(double mean, double stddev);
		double ChiSquaredDist(double degrees);
		double CauchyDist(double a, double b);
		double FisherFDist(double m, double n);
		double StudentTDist(double degrees);

	private:
		int _id;
		mt19937_64 gen;

		bernoulli_distribution _bernoulliDist;
		binomial_distribution<> _binomialDist;
		geometric_distribution<> _geometricDist;
		negative_binomial_distribution<> _negBinomialDist;
		uniform_int_distribution<> _uniIntDist;
		uniform_real_distribution<> _uniRealDist;

		poisson_distribution<> _poissonDist;
		exponential_distribution<> _expDist;
		gamma_distribution<> _gammaDist;
		weibull_distribution<> _weibullDist;
		extreme_value_distribution<> _extremeValueDist;

		normal_distribution<> _normalDist;
		lognormal_distribution<> _logNormalDist;
		chi_squared_distribution<> _chiSquaredDist;
		cauchy_distribution<> _cauchyDist;
		fisher_f_distribution<> _fisherFDist;
		student_t_distribution<> _studentTDist;

		RandomLib(int seed);
	} ;
}

#endif /* end of include guard: H_RANDOMLIB_GUARD_ */
