#ifndef H_LOGICALPROCESS_GUARD_
#define H_LOGICALPROCESS_GUARD_

#include "sim/types.hpp"
#include "sim/handlers/Taggable.h"
#include "EventHandler.hpp"

#include <vector>
#include <functional>
#include <typeindex>

#define LP_IT vector<LogicalProcess*>::iterator

using namespace std;

namespace sim {
	class EventHandler;

	class LogicalProcess : public Taggable {
	public:
		static void setState(GlobalState*, int);
		static GlobalState* getState();
		
		static LogicalProcess* getInstance(lpid_s);
		static lpid_s addLP(LogicalProcess*);
		static lpid_s currentLP();
		static void destroy();
		static int getNbLPs();
		static EventHandler* getRecyclable(type_index);

		virtual ~LogicalProcess();

		void ScheduleEvent(EventHandler*);
		void UnscheduleAllEvents();
		void UnscheduleAllEvents(int);
		
		EventHandler* TopEvent();
		simtime_s TopEventTS();
		bool HasNextEvent();
		void ExecuteTopEvent();
		
		bool IsEnd();
		bool IsReset();
		void End();
		void Reset();
		
		lpid_s GetId();
		void SetId(lpid_s);
		simtime_s GetCurrentTime();

	protected:
		LogicalProcess();
		
		virtual void CustomReset();
		
	private:
		vector<EventHandler*> _events;
		bool _end;
		bool _reset;
		simtime_s _current_time;
		lpid_s _id;
		
	} ;
}

#endif /* end of include guard: H_LOGICALPROCESS_GUARD_ */
