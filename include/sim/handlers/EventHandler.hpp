#ifndef H_EVENTHANDLER_GUARD_
#define H_EVENTHANDLER_GUARD_

#include "sim/types.hpp"
#include "sim/handlers/Taggable.h"

#define EVENT_IT vector<EventHandler*>::iterator

namespace sim
{

    class LogicalProcess;
    class GlobalState;

    class EventHandler : public Taggable
    {
    public:
        EventHandler(lpid_s, simtime_s);
        EventHandler(lpid_s, simtime_s, int);
        virtual ~EventHandler();

        /** This function handles the event.
         *
         * @return true to exit, false to continue
         */
        virtual void Execute(LogicalProcess*, GlobalState*);

        lpid_s GetSchedulerLP();
        void SetSchedulerLP(lpid_s);
        
        simtime_s GetTimestamp();
        void SetTimestamp(simtime_s);
        
    protected:
        simtime_s now;
        lpid_s scheduler_process;
    };
}

#endif /* end of include guard: H_EVENTHANDLER_GUARD_  */
