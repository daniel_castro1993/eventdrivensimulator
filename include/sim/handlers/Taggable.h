#ifndef H_TAGGABLE_GUARD_
#define H_TAGGABLE_GUARD_

namespace sim
{

    class Taggable
    {
    public:
        Taggable();
        Taggable(int);
        virtual ~Taggable();

        int GetTags();
        void SetTags(int);
        void AddTags(int);
        void RemoveTags(int);
        bool HasAnyTags(int);

    protected:
        int tags;
    };
}

#endif /* end of include guard: H_TAGGABLE_GUARD_ */

