#ifndef H_STATS_GUARD_
#define H_STATS_GUARD_

namespace sim {

	typedef struct _avg_s {
		double avg; // average of the value
		double sq_stddev; // square of the standard deviation
		int count; // number of samples taken

		_avg_s();
		_avg_s(const _avg_s&);
		~_avg_s();

		void addNewVal(const double& new_val);
		void reset();

		_avg_s& operator+=(const double& new_val);
	} avg_s;
}

#endif /* end of include guard: H_STATS_GUARD_ */
